#!/bin/bash
touch /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
touch /etc/vconsole.conf
echo "KEYMAP=us\nFONT=gr928-8x16-thin" >> /etc/vconsole.conf
ln -s /usr/share/zoneinfo/Asia/Bangkok /etc/localtime
hwclock --systohc
echo "What Host name ?"
read -r varhostname
echo "$varhostname" > /etc/hostname

pacman -S linux-lts grub-bios os-prober ntfs-3g
grub-install --target=i386-pc --recheck /dev/sda
cp /usr/share/locale/en\@quot/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo
grub-mkconfig -o /boot/grub/grub.cfg
pacman -S lightdm-gtk-greeter firefox plasma kde-applications
pacman -S optimus-manager
sudo systemctl enable optimus-manager.service
sudo systemctl enable lightdm.service
sudo systemctl enable NetworkManager.service
echo "What username ?"
read -r username
useradd -m "$username"
passwd
visudo
